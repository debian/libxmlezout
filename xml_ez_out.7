.\" This man page is Copyright (C) 2009 McKae Technologies
.\" Permission is granted to distribute possibly modified copies
.\" of this page provided the header is included verbatim,
.\" and in case of nontrivial modification author and date
.\" of the modification is added to the header.
.\"
.\" Created, 2009-12-27, Marc A. Criley, <mc@mckae.com>
.\"
.TH XML_EZ_Out  7 2009-12-27 "Linux-XML_EZ_Out" "Linux-XML_EZ_Out"
.SH NAME
XML_EZ_Out \- Simple Ada utility package for generating XML output.
.SH SYNOPSIS
.B with McKae.XML.EZ_Out.Generic_Medium;
.br
.B with McKae.XML.EZ_Out.String_Stream;
.br
.B with McKae.XML.EZ_Out.Text_File;

.HP
.B package
.IB Ezsi
.B is new Mckae.Xml.Ez_Out.Generic_Medium
.br
.B (Output_Medium =>
.IB "File_Type",
.br
.B " Put           =>"
.IB "Put",
.br
.B " New_Line      =>"
.IB "New_Line"[,
.br
.B " Format        =>"
.IB "Spread_Indented"]);

.SH DESCRIPTION
XML_EZ_Out is a small set of packages intended to aid the creation of
XML-formatted output from within Ada programs.  It basically wraps the
tags and data provided to it with XML syntax and writes them to a
user-supplied medium.

This medium can be any sort of writable entity, such as a file, a
memory buffer, or even a communications link, such as a socket.  The
only functionality required of the medium is that it supply a
meaningful "Put" (for writing a string) and "New_Line" procedure.

XML_EZ_Out package instantiations are explicitly designed to be made
directly visible with the aid of a "use" clause. Declining to use a
"use" will make using XML_EZ_Out inordinately verbose and awkward.

.SS Generic Specification
.nf
generic

   type Output_Medium is limited private;
   -- Output_Medium is whatever entity is going to received the formatted
   -- XML output. It can be a file, a stream, a buffer, a socket, whatever.
   -- All interaction with it takes place solely through the supplied Put and
   -- New_Line procedures, which are modeled after the Ada.Text_IO procedures.

   with procedure Put(F : in Output_Medium;
                      S : in String) is <>;
   -- Procedure writing a string to the instantiating output medium.

   with procedure New_Line
	 (F       : in Output_Medium;
	  Spacing : in Ada.Text_IO.Positive_Count := 1) is <>;
   -- Procedure writing a line break to the instantiating output medium.

   Format : Formatting_Options := Spread_Indented;
   -- Specify whether the XML being written is to be indented, i.e. be
   -- "prettified". (DEPRECATED, control formatting by setting the
   -- Current_Format variable available in the package spec.)

   Max_Element_Nesting : Positive := 200;
   -- The maximum element nesting depth of an XML document (used to set
   -- size of tag checking stack).

package McKae.XML.EZ_Out.Generic_Medium;
.fi

.SS Variables
.TP
.B Current_Format
The indentation format of the XML that is utilized.  This can be
altered at any time. Available settings are
.BR Continuous_Stream
and
.BR Spread_Indented ". (Supersedes deprecated generic parameter " Format ".)"

.TP
.B Default_Output_Null_Attributes
Boolean indication whether to output an attribute if it has a null value.


.SS Package Operations
See the 
.B Generic_Medium
package specification for details of parameters and overloaded variations.
.TP
.B Output_XML_Header
Procedure to output a standard XML header line, as amended by the supplied
arguments.  To omit a standard attribute, pass an empty string as its value.

.TP
.B Output_Processing_Instruction
Add a processing instruction to the XML document.

.TP
.B Output_Element
Generate an entire element designated with the given tag and
containing the provided content and attribute values.

.TP
.B Output_Tag
Generate an element tag containing zero or more attributes.  By
default the element is created using the compact, no-end-tag
notation; to force generation of an element that has both start
and end tags while containing no content, set End_Tag to True.

.TP
.B Start_Element
Initiate the generation of an XML element with the given tag and
zero or more attribute values.

.TP
.B End_Element
Indicate the completion of the output of an XML element.  If a
Tag is specified, compare it against the element tag that is
currently open, and raise Element_End_Mismatch if the two do
not match.  If there is no open element, then raise
Element_Not_Open.

.TP
.B Output_Content
Place the text, as is, as the content of the currently open XML
element.  Output_Content can be called repeatedly, and will
simply continue to append the additional content.  If there is
no open element, raise Element_Not_Open.

.TP
.B "="
Associate an attribute name with a value. There are several overloaded
variations of the "=" function for
defining attribute name/value pairs. For each, the attribute name,
.BR Attr,
can be of either the predefined Ada String type or the 
Unbounded_String type from Ada.Strings.Unbounded. The provided "="
functions accept a
.BR Value
of a predefined Integer-based, Float-based,
String, or Character type.

.SS Errors
.TP
.B Element_Not_Open
An attempt was made to end, or add content to, an element when
there were no open elements awaiting text or completion.

.TP
.B Element_End_Mismatch
The specified end tag does not match that of the currently open
element.

.TP
.B Nesting_Too_Deep
The number of open, nested elements has exceeded the maximum
level that was specified.  Consider increasing the value of the 
Max_Element_Nesting generic parameter.

.TP
.B Invalid_Construction
An attempt was made to create a malformed document, such as
inserting a process instruction into an open element.

.SS Auxiliary Packages
.TP
.B McKae.XML.EZ_Out.Text_File
Mckae.XML.EZ_Out.Generic_Medium pre-instantiated with the
 predefined Ada.Text_IO.File_Type.

.TP
.B Mckae.XML.EZ_Out.String_Stream
McKae.XML.EZ_Out.Generic_Medium pre-instantiated with a limited 
functionality in-memory string buffering package, nested within the
Mckae.XML.EZ_Out.String_Stream package as
.BR String_Buffering.

.SH EXAMPLES
The key facilitator of making XML_EZ_Out usage readable when
generating XML documentation is the overloading of a number of
variations of the "=" function.  By doing this, a simple XML element
having no content, such as:

<player lastName="Cuddyer" firstName="Michael" team="Twins"/>

can be generated as:
.nf
Output_Tag(F,
           "player",
           ("lastName"  = "Cuddyer",
            "firstName" = "Michael",
            "team"      = "Twins"));
.fi

To simplify the specification of the attributes, variations of "=" are
provided.  Given these declarations:
.nf
    Batting_Average : Float;
    At_Bats         : Natural;
.fi
One can directly reference the variables:

.nf
Output_Tag(F,
           "stats",
           ("battingAvg" = Batting_Average,
            "atBats"     = At_Bats));
.fi

.SH NOTES
XML_EZ_Out is designed in such a way that
instantiations of it are meant to be "used" by the application.
When accompanied with a "use" clause, specifying the XML to be
produced is very simple and clear.  Insisting on using qualified
names will make for very obtuse code.  Likewise, "named
parameter" notation would obscure the complementarity of the Ada
and XML, so apply for a waiver from any such style standards.

The medium must be open and ready to accept content before
invoking any of these XML output subprograms.

If the medium raises any exceptions as a result of invoking the
supplied Put or New_Line procedures, those exceptions will be
passed through to the caller.


.SH AUTHOR
Marc A. Criley, McKae Technologies (mc@mckae.com)

.SH CAVEATS
XML_EZ_Out does no validation of the XML content it is
being asked to output, and it is possible to generate malformed
documents.  That includes the handling of character encoding.  While
XML_EZ_Out will claim the document is "UTF-8" or otherwise as set by
the application, it is up to the application to ensure that correct
content is provided in the strings that are passed to its various
subprograms.  Used appropriately, though, it can provide a clear and
readable means to aid the dynamic generation of XML content by Ada
programs.
